﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;


namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    /// <summary>
    /// Репозиторий, хранящий объекты в памяти
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        #region состояние класса
        private IAsyncEnumerable<T> _data { get; set; } 
        #endregion

        #region свойства
        /// <summary>
        /// Коллеция объектов
        /// </summary>
        protected IAsyncEnumerable<T> Data 
        {
            get 
            {
               // _data ??= new List<T>();

                return _data; 
            }

            set 
            { 
                _data = value; 
            } 
        }
        #endregion

        #region конструктор
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="data">список объектов, которые надо разместить в репозитории</param>
        public InMemoryRepository(IEnumerable<T> data)
        {
            if (data != null)
            {
                Data = data.ToAsyncEnumerable();
            }
        }
        #endregion

        #region реализация IRepository
        /// <summary>
        /// Получить все записи
        /// </summary>
        /// <returns></returns>
        /// <remarks>метод асинхронных</remarks>
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.Run(() =>
            {
                return Data.ToEnumerable();
            });
        }

        /// <summary>
        /// Получить запись по идентификатору
        /// </summary>
        /// <param name="id">идентификатор сущноси</param>
        /// <returns></returns>
        /// <remarks>метод асинхронный</remarks>
        public async Task<T> GetByIdAsync(Guid id)
        {
            return await Data.FirstOrDefaultAsync(x => x.Id == id);
        }

        /// <summary>
        /// Создать новый элемент сущности
        /// </summary>
        /// <param name="entity">данные для создаваемого объекта</param>
        /// <remarks>метод асинхронный</remarks>
        public async Task<bool> CreateAsync(T entity)
        {
            if (entity == null)
            {
                throw new RepositoryException("Передан пустой объект для вставки в репозиторий");
            }

            entity.Id = Guid.NewGuid();

            Data = await Task.FromResult(Data.Append(entity));

            return true;
        }

        /// <summary>
        /// Обновить элемент сущности
        /// </summary>
        /// <param name="entity">данные для создаваемого объекта</param>
        /// <remarks>метод асинхронный</remarks>
        public async Task<bool> UpdateAsync(T entity)
        {
            if (entity == null)
            {
                throw new RepositoryException("Передан пустой объект для вставки в репозиторий");
            }

            T existEntity = await Data.WhereAwait(async x => await Task.FromResult(x.Id == entity.Id)).FirstOrDefaultAsync()
                ?? throw new RepositoryException($"В репозитории объектов типа {typeof(T)} не найдено для обновление объетк с Id {entity.Id}");

            existEntity.FillBy(entity);

            return true;

        }

        /// <summary>
        /// Удалить элемент сущности
        /// </summary>
        /// <param name="entity">данные для создаваемого объекта</param>
        /// <remarks>метод асинхронный</remarks>
        public async Task<bool> DeleteAsync(T entity)
        {
            if (entity == null)
            {
                throw new RepositoryException("Передан пустой объект для вставки в репозиторий");
            }

            int countItemBeforeDelete = await Data.CountAsync();

            Data = Data.WhereAwait(async x => await Task.FromResult(x.Id != entity.Id));

            int countItemAfterDelete = await Data.CountAsync();

            return countItemAfterDelete < countItemBeforeDelete;

        }

        /// <summary>
        /// Удалить элемент сущности по идентификатору
        /// </summary>
        /// <param name="id">идентификатор удаляемого объекта</param>
        /// <remarks>метод асинхронный</remarks>
        public async Task<bool> DeleteByIdAsync(Guid id)
        {
            int countItemBeforeDelete = await Data.CountAsync();

            Data = Data.WhereAwait(async x => await Task.FromResult(x.Id != id));

            int countItemAfterDelete = await Data.CountAsync();

            return countItemAfterDelete < countItemBeforeDelete;
        }
        #endregion
    }
}