﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    /// <summary>
    /// Интерфейс репозитория
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IRepository<T>
        where T: BaseEntity
    {
        /// <summary>
        /// Получить все записи
        /// </summary>
        /// <returns></returns>
        /// <remarks>метод асинхронных</remarks>
        Task<IEnumerable<T>> GetAllAsync();
        
        /// <summary>
        /// Получить запись по идентификатору
        /// </summary>
        /// <param name="id">идентификатор сущноси</param>
        /// <returns></returns>
        /// <remarks>метод асинхронный</remarks>
        Task<T> GetByIdAsync(Guid id);

        /// <summary>
        /// Создать новый элемент сущности
        /// </summary>
        /// <param name="entity">данные для создаваемого объекта</param>
        /// <remarks>метод асинхронный</remarks>
        Task<bool> CreateAsync(T entity);

        /// <summary>
        /// Обновить элемент сущности
        /// </summary>
        /// <param name="entity">данные для создаваемого объекта</param>
        /// <remarks>метод асинхронный</remarks>
        Task<bool> UpdateAsync(T entity);

        /// <summary>
        /// Удалить элемент сущности
        /// </summary>
        /// <param name="entity">данные для создаваемого объекта</param>
        /// <remarks>метод асинхронный</remarks>
        Task<bool> DeleteAsync(T entity);

        /// <summary>
        /// Удалить элемент сущности по идентификатору
        /// </summary>
        /// <param name="id">идентификатор удаляемого объекта</param>
        /// <remarks>метод асинхронный</remarks>
        Task<bool> DeleteByIdAsync(Guid id);
    }
}