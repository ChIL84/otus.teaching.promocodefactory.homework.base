﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    /// <summary>
    /// Исключение, возникшее при работе с репозиторием
    /// </summary>
    public class RepositoryException : 
        Exception
    {
        #region конструктор
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="message">сообщение об ошибке</param>
        /// <param name="innerException">вложенное исключение</param>
        public RepositoryException(string message, Exception innerException) :
            base(message, innerException)
        { }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="message">сообщение об ошибке</param>        
        public RepositoryException(string message) :
            base(message)
        { }
        #endregion
    }
}
