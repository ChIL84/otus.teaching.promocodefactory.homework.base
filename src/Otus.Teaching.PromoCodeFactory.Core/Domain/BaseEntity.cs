﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain
{
    public abstract class BaseEntity
    {
        public Guid Id { get; set; }

       abstract public void FillBy(BaseEntity entity);
    }
}