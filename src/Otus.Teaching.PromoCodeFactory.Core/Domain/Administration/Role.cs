﻿using System;
using System.Data;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Role
        : BaseEntity
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public override void FillBy(BaseEntity entity)
        {
            Role source = entity as Role;

            if (source == null) return;

            Name = source.Name;

            Description = source.Description;            
        }
    }
}