﻿using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Employee
        : BaseEntity
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; set; }

        public List<Role> Roles { get; set; }

        public int AppliedPromocodesCount { get; set; }

        public override void FillBy(BaseEntity entity)
        {
            Employee source = entity as Employee;

            if (source == null) return;

            FirstName = source.FirstName;

            LastName = source.LastName;

            Email = source.Email;

            AppliedPromocodesCount = source.AppliedPromocodesCount;

            Roles = source.Roles;
        }
    }
}