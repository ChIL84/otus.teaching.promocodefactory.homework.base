﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.DtoModels;
using Otus.Teaching.PromoCodeFactory.WebHost.DtoTransformer;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        #region свойства
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _roleRepository;
        #endregion

        #region конструктор
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="employeeRepository">репозиторий сотрудников</param>
        /// <param name="roleRepository">репозиторий ролей</param>
        public EmployeesController(IRepository<Employee> employeeRepository,
            IRepository<Role> roleRepository)
        {
            _employeeRepository = employeeRepository;
            _roleRepository = roleRepository;
        } 
        #endregion

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<DtoEmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                x.ToDtoEmployeeShortResponse()).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<DtoEmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
            {
                return NotFound();
            }

            return employee.ToDtoEmployeeResponse();
        }

        /// <summary>
        /// Добавить сотрудника
        /// </summary>
        /// <param name="dtoEmployeeRequest">информация о сотруднике</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateEmployee(DtoEmployeeRequest dtoEmployeeRequest)
        {
            if (dtoEmployeeRequest == null)
            {
                ModelState.AddModelError("", "Передан пустой объект для вставки");

                return UnprocessableEntity(ModelState);
            }
            
            Employee employee = dtoEmployeeRequest.ToEmployee(_roleRepository,
                out List<KeyValuePair<string,string>> transformerErrors);

            if (transformerErrors != null
                && transformerErrors.Count > 0)
            {
                foreach (KeyValuePair<string, string> error in transformerErrors)
                {
                    ModelState.AddModelError(error.Key, error.Value);
                }
            }

            if (!ModelState.IsValid)
            {
                return UnprocessableEntity(ModelState);
            }

            bool isCreated = await _employeeRepository.CreateAsync(employee);

            if(!isCreated) return BadRequest();

            return Ok();
        }

        /// <summary>
        /// Обновить информацию о сотруднике
        /// </summary>
        /// <param name="id">идентификатор сотрудника</param>
        /// <param name="dtoEmployeeRequest">информация о сотруднике</param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateEmployee(Guid id, DtoEmployeeRequest dtoEmployeeRequest)
        {
            if (dtoEmployeeRequest == null)
            {
                ModelState.AddModelError("", "Передан пустой объект для вставки");

                return UnprocessableEntity(ModelState);
            }

            Employee employee = dtoEmployeeRequest.ToEmployee(_roleRepository,
                out List<KeyValuePair<string, string>> transformerErrors);

            if (transformerErrors != null
                && transformerErrors.Count > 0)
            {
                foreach (KeyValuePair<string, string> error in transformerErrors)
                {
                    ModelState.AddModelError(error.Key, error.Value);
                }
            }

            if (!ModelState.IsValid)
            {
                return UnprocessableEntity(ModelState);
            }

            employee.Id = id;

            bool isCreated = await _employeeRepository.UpdateAsync(employee);

            if (!isCreated) return BadRequest();

            return Ok();
        }

        /// <summary>
        /// Удалить информацию о сотруднике
        /// </summary>
        /// <param name="id">идентификатор сотрудника</param>        
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteEmployee(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
            {
                return NotFound();
            }

            bool deleteResult = await _employeeRepository.DeleteAsync(employee);

            if (!deleteResult) return BadRequest();

            return Ok();
        }
    }
}