﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.DtoModels;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.DtoTransformer
{
    /// <summary>
    /// Класс преобразования DtoEmployeeRequest в объекты других классов
    /// </summary>
    public static class TransformerDtoEmployeeRequest
    {
        /// <summary>
        /// Метод расширения для преобразования в Employee
        /// </summary>
        /// <param name="dtoEmployeeRequest"></param>
        /// <param name="roleRepository"></param>
        /// <param name="transformerErrors"></param>
        /// <returns></returns>
        public static Employee ToEmployee(this DtoEmployeeRequest dtoEmployeeRequest,
            IRepository<Role> roleRepository, out List<KeyValuePair<string, string>> transformerErrors)
        {
            transformerErrors = new List<KeyValuePair<string, string>>();
            
            if (dtoEmployeeRequest == null) return null;

            List<Role> rolesList = new List<Role>();
            
            if(dtoEmployeeRequest.RoleIds != null 
                && dtoEmployeeRequest.RoleIds.Count > 0)
            {
                if (roleRepository == null)
                {
                    transformerErrors.Add(new KeyValuePair<string, string>(
                        "", 
                        "Не задан репозиторий ролей."));
                       
                    return null;
                }

                foreach(Guid roleId in  dtoEmployeeRequest.RoleIds) 
                { 
                    Role role = roleRepository.GetByIdAsync(roleId).Result;

                    if (role == null)
                    {
                        transformerErrors.Add(new KeyValuePair<string, string>(
                            nameof(dtoEmployeeRequest.RoleIds),
                            $"Не найдена роль с Id '{roleId}'"));

                        continue;
                    }

                    rolesList.Add(role);
                }
            }

            return new Employee
            {
                FirstName = dtoEmployeeRequest.FirstName,
                LastName = dtoEmployeeRequest.LastName,
                Email = dtoEmployeeRequest.Email,   
                AppliedPromocodesCount = dtoEmployeeRequest.AppliedPromocodesCount,
                Roles = rolesList
            };
        }
    }
}
