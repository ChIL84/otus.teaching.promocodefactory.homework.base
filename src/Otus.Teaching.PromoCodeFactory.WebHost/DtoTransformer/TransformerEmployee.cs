﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.DtoModels;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.WebHost.DtoTransformer
{
    /// <summary>
    /// Класс преобразования Employee в объекты других классов
    /// </summary>
    public static class TransformerEmployee
    {
        /// <summary>
        /// Метод расширения для преобразования в DtoEmployeeResponse
        /// </summary>
        /// <param name="employee"></param>        
        /// <returns></returns>
        public static DtoEmployeeResponse ToDtoEmployeeResponse(this Employee employee)
        {
            return new DtoEmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new DtoRoleItemResponse()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };
        }

        /// <summary>
        /// Метод расширения для преобразования в DtoEmployeeShortResponse
        /// </summary>
        /// <param name="employee"></param>        
        /// <returns></returns>
        public static DtoEmployeeShortResponse ToDtoEmployeeShortResponse(this Employee employee)
        {
            return new DtoEmployeeShortResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                FullName = employee.FullName,
            };
        }
    }
}
