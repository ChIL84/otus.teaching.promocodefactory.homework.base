﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.DtoModels
{
    public class DtoRoleItemResponse
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
    }
}