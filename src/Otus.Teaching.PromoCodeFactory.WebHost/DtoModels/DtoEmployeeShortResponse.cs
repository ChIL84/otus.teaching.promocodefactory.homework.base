﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.DtoModels
{
    public class DtoEmployeeShortResponse
    {
        public Guid Id { get; set; }
        
        public string FullName { get; set; }

        public string Email { get; set; }
    }
}