﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.DtoModels
{
    public class DtoEmployeeRequest
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public int AppliedPromocodesCount { get; set; }

        public List<Guid> RoleIds { get; set; }        
    }
}