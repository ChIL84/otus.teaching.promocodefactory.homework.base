﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.DtoModels
{
    public class DtoEmployeeResponse
    {
        public Guid Id { get; set; }       

        public string FullName { get; set; }

        public string Email { get; set; }

        public List<DtoRoleItemResponse> Roles { get; set; }

        public int AppliedPromocodesCount { get; set; }
    }
}